# Todo está importado en el main para usarse
import math

def comparar_numeros():
    primer_numero = int(input("Ingrese primer numero: "))
    segundo_numero = int(input("Ingrese segundo numero: "))
    if primer_numero > segundo_numero:
        print("El primer numero es mayor")
    elif primer_numero < segundo_numero:
        print("El segundo numero es mayor")
    else:
        print("Son de igual valor")


def verificar_entero(numero):
    try:
        if numero > 0:
            print("Es positivo")
        else:
            print("Es negativo")
    except:
        print("Esto solo permite ingresar números")


def calculadora():
    num1 = int(input("Ingrese el primer numero: "))
    num2 = int(input("Ingrese el segundo numero: "))
    operador = input("Elija su operación con el signo correspondiente: +, -, *, / :")
    def suma():
        return num1 + num2
    def resta():
        return num1 - num2
    def multiplica():
        return num1 * num2
    def divide():
        return num1 / num2
    if operador == "+":
        resultado = suma()
        print(resultado)
    if operador == "-":
        resultado = resta()
        print(resultado)
    if operador == "*":
        resultado = multiplica()
        print(resultado)
    if operador == "/":
        resultado = divide()
        print(resultado)
    else:
        print("ERROR: Por favor, ingrese un numero válido para la operación")
        calculadora()


def pitagoras(cateto1, cateto2):
    resultado_pitagoras = math.sqrt((cateto1 ** 2) + (cateto2 ** 2))
    print("Hipotenusa: ")
    return resultado_pitagoras


def area_circunferencia(radio):
    return (3.14 * (radio ** 2))

def login_admin():
    login_tries = 4
    while login_tries != 0:
        USER_ADMIN = "Admin"
        PASS_ADMIN = "adMin"
        print("Bienvenido a su login de usuario")
        user_input = input("Ingrese su usuario: ")
        pass_input = input("Ingrese su contraseña: ")
        if user_input == USER_ADMIN and pass_input == PASS_ADMIN:
            print("Bienvenid@ al sistema!")
        else:
            login_tries -= 1
            print(f"Credenciales incorrectas. Quedan {login_tries} intentos")
            if login_tries == 0:
                print("El sistema explotó")




login_admin()